//
//  ViewController.m
//  Jumblify
//
//  Created by Andy Obusek on 10/11/14.
//  Copyright (c) 2014 Tuts+. All rights reserved.
//

#import "JumblifyViewController.h"
#import <Accelerate/Accelerate.h>
#import <math.h>

@interface JumblifyViewController ()

@end

@implementation JumblifyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
//   NSLog(@"%@", [self sieve:20]);
//    NSLog(@"%d", [self greatestCommonDivisorForA:2336 andB:1314]);
//    NSLog(@"%d", [self isUniqueString:@"kkatee"]);
//    NSLog(@"%@", [self reverseString:@"kate"]);
//    NSLog(@"%d", [self stringIsAPermutationOfAnother:@"kate" andSecond:@"etak"]);
//    NSLog(@"%@", [self compressString:@"AAAABBBCCCCCEEFGIIJJJKLMNNNNOOO"]);
//    //
//    NSMutableArray *dataArray = [[NSMutableArray alloc] initWithCapacity: 3];
//    
//    [dataArray insertObject:[NSMutableArray arrayWithObjects:@1, @2, @3, @4, nil] atIndex:0];
//    [dataArray insertObject:[NSMutableArray arrayWithObjects:@5, @6, @7, @8, nil] atIndex:1];
//    [dataArray insertObject:[NSMutableArray arrayWithObjects:@9, @10, @11, @12, nil] atIndex:2];
//    [dataArray insertObject:[NSMutableArray arrayWithObjects:@13, @14, @15, @16, nil] atIndex:3];
//    NSLog(@"%@", dataArray[1]);
//    [self rotateMatrix:dataArray andCount:4];
//    NSMutableArray *mqtrix = [NSMutableArray new];
//    [mqtrix insertObject:[NSMutableArray arrayWithObjects:@1, @2, @0, nil] atIndex:0];
//    [mqtrix insertObject:[NSMutableArray arrayWithObjects:@5, @0, @7, nil] atIndex:1];
//    [mqtrix insertObject:[NSMutableArray arrayWithObjects:@9, @10, @11, nil] atIndex:2];
//    [self findZeroElementInMatrix:mqtrix];
//    [self firstString:@"waterbottle" isRotationOfSecond:@"erbottlewat"];
    [self heavyIntegers:8675 and:8689];
    [self returnMAx:[@[@1, @1, @0, @1, @0, @0, @1, @1] mutableCopy]];
}

-(int)returnMAx:(NSMutableArray *)A {
    int result = 0;
    if (A.count == 1 ) {
        return result;
    }
    if (A.count == 2 && A[0] == A[1]) {
        return result;
    }
    NSMutableArray *arrayOfResults = [NSMutableArray new];
    for (int j = 0; j < A.count; j++) {
        NSMutableArray *changedA = [A mutableCopy];
        int counter = 0;
        changedA[j] = [NSNumber numberWithBool:![changedA[j] boolValue]];
        for (int i = 0 ; i < changedA.count - 1; i++) {
            if ([changedA[i] integerValue] == [changedA[i+1] integerValue]) {
                counter ++;
            }
        }
        [arrayOfResults addObject:[NSNumber numberWithInt:counter]];
    }
    
    for (NSNumber *number in arrayOfResults) {
        if (result < [number intValue]) {
            result = [number intValue];
        }
    }
    
        return result;
}

-(int)heavyIntegers:(int)A and:(int)B{
    int counter = 0;
    for (int i = A; i <= B; i++) {
        float sum = 0;
        int digit, n;
        n = i;
        float length = (n ==0) ? 1 : (int)log10(n) + 1;
        while (n) {
            digit = n % 10;
            n /= 10;
            sum+=digit;
        }
        if (sum / length > 7) {
            counter ++;
        }
    }
    return counter;
}



- (BOOL)isUniqueString:(NSString *)inputString {
    if (inputString.length > 256) {
        return NO;
    }
    
    NSMutableArray *alphabet = [NSMutableArray new];
    for (int i = 0; i < 256; i++) {
        [alphabet addObject:[NSNumber numberWithBool:NO]];
    }
    for (int i = 0; i < inputString.length; i++) {
        int val = [inputString characterAtIndex:i];
        if([alphabet[val] boolValue]) {
            return NO;
        } else {
            alphabet[val] = [NSNumber numberWithBool:YES];
        }
    }
    return YES;
}

-(NSString *)reverseString:(NSString *)input {
    NSString *reverse = @"";
    for (NSInteger i = input.length - 1; i >= 0; i--) {
        NSString *character = [NSString stringWithFormat:@"%C", [input characterAtIndex:i]];
        reverse = [reverse stringByAppendingString:character];
      //  NSLog(@"%@", reverse);
    }
    return reverse;
}

-(BOOL)stringIsAPermutationOfAnother:(NSString *)first andSecond:(NSString *)second {
    if (first.length != second.length) {
        return NO;
    }
    
    if ([[self reverseString:first] isEqualToString:second]) {
    //if ([[self reverseString:first] caseInsensitiveCompare:second] == NSOrderedSame) { // insensitive case
        return YES;
    } else {
        return NO;
    }
}

-(NSString *)compressString:(NSString *)input {
    NSInteger count = 1;
    NSString *compressedStr = @"";
    for (NSInteger i = 0; i < input.length - 1; i++) {
        if ([input characterAtIndex:i] == [input characterAtIndex:i+1]) {
            count++;
        } else {
            NSString *temp = [NSString stringWithFormat:@"%C%ld", [input characterAtIndex:i], (long)count];
            compressedStr = [compressedStr stringByAppendingString:temp];
            count = 1;
        }
    }
    if (compressedStr.length < input.length) {
        return compressedStr;
    } else {
        return input;
    }
}

-(void)rotateMatrix:(NSArray *)matrix andCount:(int)n{
    for (int layer = 0; layer < n / 2; layer++) {
        int first = layer;
        int last = n - 1 - layer;
        for (int i = first; i < last; i++) {
            int offset = i - first;
            
            int top = [matrix[first][i] intValue];
            // l -> t
            matrix[first][i] = matrix[last - offset][first];
            //b -> l
            matrix[last - offset][first] = matrix[last][last - offset];
            //r -> b
            matrix[last][last - offset] = matrix[i][last];
            //top -> r
            matrix[i][last] = [NSNumber numberWithInt:top];
        }
    }
    
    NSLog(@"%@", matrix[1][1]);
}

-(void)findZeroElementInMatrix:(NSArray *)matrix {

    NSMutableArray *row = [NSMutableArray new];
    NSMutableArray *column = [NSMutableArray new];
    //initializing arrays
    for (int i = 0; i < matrix.count; i++) {
        [row addObject:[NSNumber numberWithBool:NO]];
    }
    for (int j = 0; j < [matrix[0] count]; j++) {
        [column addObject:[NSNumber numberWithBool:NO]];
    }
    //store the row and column index with value 0
    for (int i = 0; i < matrix.count; i++) {
        for (int j = 0; j < [matrix[0] count]; j++) {
            if ([matrix[i][j] intValue] == 0) {
                row[i] = [NSNumber numberWithBool:YES];
                column[j] = [NSNumber numberWithBool:YES];
            }
        }
    }
    //nullify rows
    for (int k = 0; k < row.count; k++) {
        if ([row[k] boolValue]) {
            for (int z = 0; z < [matrix[0] count]; z++) {
                matrix[k][z] = [NSNumber numberWithInt:0];
            }
        }
    }
    //nullify columns
    for (int k = 0; k < column.count; k++) {
        if ([column[k] boolValue]) {
            for (int z = 0; z < [matrix count]; z++) {
                matrix[z][k] = [NSNumber numberWithInt:0];
            }
        }
    }
    NSLog(@"%@", matrix);
}

-(BOOL)firstString:(NSString *)str1 isRotationOfSecond:(NSString *)str2{
    NSUInteger len = str1.length;
    if (len == str2.length && len > 0) {
        NSString *str1str1 = [NSString stringWithFormat:@"%@%@", str1, str1];
        if([str1str1 rangeOfString:str2].location != NSNotFound) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - Just Maths

-(NSArray *)sieve:(int )n { // The Sieve of Eratosthenes implementation
    NSMutableArray *prime = [NSMutableArray new];
    [prime addObject:[NSNumber numberWithBool:NO]];
    [prime addObject:[NSNumber numberWithBool:NO]];
    for (int i = 2; i < n; i++) {
        [prime addObject:[NSNumber numberWithBool:YES]];
    }

    int m = sqrt(n);
    
    for (int i=2; i<=m; i++)
        if (prime[i])
            for (int k=i*i; k<=n; k+=i)
                prime[k] = [NSNumber numberWithBool:NO];
    
    return prime;
}

//assume that a and b cannot both be 0
-(int)greatestCommonDivisorForA:(int)a andB:(int)b {
    if (b == 0)
        return a;
    
    return [self greatestCommonDivisorForA:b andB:(a % b)];
}

@end
